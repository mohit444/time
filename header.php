<?php
?>
<html>
<head>
    <title>Time.com</title>
    <link type="text/css" rel="stylesheet" href="style.css">
</head>
<body>
<div id="bodyborder">
    <header id="head">Time.com </header>
    <div id="indiautc">INDIA (DELHI) UTC+5:30</div>
    <div id="gmt">Greenwich Mean Time (GMT) now</div>
    <div id="gmttime">
        <?php
        date_default_timezone_set('GMT');
        echo date("d/m/y h:i:s");
        ?>
    </div>
</div>
</body>
</html>
